server:
	go run cmd/main.go

build:
	go build -o bin/server cmd/main.go

d.up:
	sudo docker-compose up

d.down:
	sudo docker-compose down

d.up.build:
	sudo docker-compose up -d --build