package users

import (
	"go-fiber-api-docker/pkg/common/models"

	"github.com/gofiber/fiber/v2"
)

type AddUserRequestBody struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

func (h handler) Add(c *fiber.Ctx) error {
	body := AddUserRequestBody{}

	// parse body, attach to AddProductRequestBody struct
	if err := c.BodyParser(&body); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	}

	var user models.User

	user.Login = body.Login
	user.Password = body.Password

	// insert new db entry
	if result := h.DB.FirstOrCreate(&user, models.User{Login: user.Login}); result.Error != nil {
		return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
	}

	return c.Status(fiber.StatusCreated).JSON(&user)
}
