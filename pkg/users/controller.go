package users

import (
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

type handler struct {
	DB *gorm.DB
}

func RegisterRoutes(app *fiber.App, db *gorm.DB) {
	h := &handler{
		DB: db,
	}

	products := app.Group("/users")
	products.Post("/", h.Add)
	products.Get("/:id", h.Get)
	products.Get("/", h.GetAll)
}
