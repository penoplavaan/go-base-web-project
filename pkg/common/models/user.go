package models

type User struct {
	Id       int    `json:"id" gorm:"primaryKey"`
	Login    string `json:"login"`
	Password string `json:"password"`
}
