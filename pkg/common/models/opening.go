package models

type Opening struct {
	Id               int    `json:"id" gorm:"primaryKey"`
	UserId           string `json:"userId"`
	Name             string `json:"name"`
	Movements        string `json:"movements"`
	AttachedVariants string `json:"attachedVariants"`
	MovementsTextual string `json:"movementsTextual"`
	SubName          string `json:"subName"`
	Side             string `json:"side"`
}
