package router

import (
	"go-fiber-api-docker/pkg/openings"
	"go-fiber-api-docker/pkg/products"
	"go-fiber-api-docker/pkg/users"

	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

func RegisterRoutes(app *fiber.App, db *gorm.DB) {
	products.RegisterRoutes(app, db)
	openings.RegisterRoutes(app, db)
	users.RegisterRoutes(app, db)
}
