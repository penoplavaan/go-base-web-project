package openings

import (
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

type handler struct {
	DB *gorm.DB
}

func RegisterRoutes(app *fiber.App, db *gorm.DB) {
	h := &handler{
		DB: db,
	}

	openings := app.Group("/openings")
	openings.Post("/", h.Add)
	openings.Get("/", h.GetAll)
	openings.Get("/:id", h.Get)
	openings.Put("/:id", h.Update)
	openings.Delete("/:id", h.Delete)
}
