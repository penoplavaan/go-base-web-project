package openings

import (
	"go-fiber-api-docker/pkg/common/models"

	"github.com/gofiber/fiber/v2"
)

func (h handler) Delete(c *fiber.Ctx) error {
	id := c.Params("id")

	var opening models.Opening

	if result := h.DB.First(&opening, id); result.Error != nil {
		return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
	}

	// delete product from db
	h.DB.Delete(&opening)

	return c.SendStatus(fiber.StatusOK)
}
