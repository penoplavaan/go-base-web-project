package openings

import (
	"go-fiber-api-docker/pkg/common/models"

	"github.com/gofiber/fiber/v2"
)

type UpdateOpeningRequestBody struct {
	Name             string `json:"name"`
	Movements        string `json:"movements"`
	AttachedVariants string `json:"attachedVariants"`
	MovementsTextual string `json:"movementsTextual"`
	SubName          string `json:"subName"`
	Side             string `json:"side"`
}

func (h handler) Update(c *fiber.Ctx) error {
	id := c.Params("id")
	body := UpdateOpeningRequestBody{}

	// getting request's body
	if err := c.BodyParser(&body); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	}

	var opening models.Opening

	if result := h.DB.First(&opening, id); result.Error != nil {
		return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
	}

	opening.Name = body.Name
	opening.Movements = body.Movements
	opening.AttachedVariants = body.AttachedVariants
	opening.MovementsTextual = body.MovementsTextual
	opening.SubName = body.SubName
	opening.Side = body.Side

	// save product
	h.DB.Save(&opening)

	return c.Status(fiber.StatusOK).JSON(&opening)
}
