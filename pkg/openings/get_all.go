package openings

import (
	"go-fiber-api-docker/pkg/common/models"

	"github.com/gofiber/fiber/v2"
)

type GetOpeningsFilteredRequestBody struct {
	Side   string `json:"side"`
	UserId string `json:"user_id"`
}

func (h handler) GetAll(c *fiber.Ctx) error {
	queryString := GetOpeningsFilteredRequestBody{}
	query := h.DB

	if err := c.QueryParser(&queryString); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	}

	if len(queryString.Side) > 0 {
		query = query.Where("side = ?", queryString.Side)
	}
	if len(queryString.UserId) > 0 {
		query.Where("user_id", queryString.UserId)
	}

	var openings []models.Opening
	if result := query.Find(&openings); result.Error != nil {
		return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
	}

	return c.Status(fiber.StatusOK).JSON(&openings)
}
