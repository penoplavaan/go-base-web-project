package openings

import (
	"go-fiber-api-docker/pkg/common/models"

	"github.com/gofiber/fiber/v2"
)

type AddOpeningRequestBody struct {
	Name             string `json:"name"`
	Movements        string `json:"movements"`
	AttachedVariants string `json:"attachedVariants"`
	MovementsTextual string `json:"movementsTextual"`
	SubName          string `json:"subName"`
	Side             string `json:"side"`
	UserId           string `json:"userId"`
}

func (h handler) Add(c *fiber.Ctx) error {
	body := AddOpeningRequestBody{}

	// parse body, attach to AddProductRequestBody struct
	if err := c.BodyParser(&body); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	}

	var opening models.Opening

	opening.Name = body.Name
	opening.Movements = body.Movements
	opening.MovementsTextual = body.MovementsTextual
	opening.SubName = body.SubName
	opening.Side = body.Side
	opening.UserId = body.UserId
	opening.AttachedVariants = body.AttachedVariants

	// insert new db entry
	if result := h.DB.Create(&opening); result.Error != nil {
		return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
	}

	return c.Status(fiber.StatusCreated).JSON(&opening)
}
