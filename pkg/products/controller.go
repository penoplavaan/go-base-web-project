package products

import (
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

type handler struct {
	DB *gorm.DB
}

func RegisterRoutes(app *fiber.App, db *gorm.DB) {
	h := &handler{
		DB: db,
	}

	products := app.Group("/products")
	products.Post("/", h.Add)
	products.Get("/", h.GetAll)
	products.Get("/:id", h.Get)
	products.Put("/:id", h.Update)
	products.Delete("/:id", h.Delete)
}
